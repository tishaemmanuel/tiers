#!/bin/bash

function uninstall_gems() {
  # Uninstall gems in order
  echo 3 | gem uninstall dbd-mysql
  echo 3 | gem uninstall dbi
  echo 3 | gem uninstall mysql2
  echo 3 | gem uninstall mysql
}
function install_deps() {
  # On logstash_7, mysql is installed as version 5.5, while a few mysql libraries (like mysql-common) are still
  # 5.1. This results in conflicts. We first remove all the traces of mysql51 and then reinstall 5.5 packages
  yum remove -y mysql51*
  yum install -y mysql mysql-devel
  yum install -y curl curl-devel

  # Install pip
  easy_install pip

  # Python mysql is corrupted too. Reinstall it
  echo y | pip uninstall mysql-python
  pip install mysql-python
}
function install_rvm() {
su - ec2-user <<EOF
  # Install gpg keys for the user
  gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  curl -L get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm
  rvm requirements

  # Install Ruby version
  rvm install 2.1
  rvm alias create ruby21 2.1

  rvm use ruby21
  # Do bundle install
  cd /tmp/tapp
  bundle install --without=development test

  # Set system ruby as defaut
  rvm use system --default
EOF
}

function patch_vulnerabilities() {
    yum update -y glibc  # upgrade glibc for CVE-2015-0235
    yum update -y bash
    yum update -y openssl
}

uninstall_gems
install_deps
patch_vulnerabilities

# Snappy libs required for hadoop2
yum install -y snappy snappy-devel

#install new nginx
gem install passenger -v 4.0.53
passenger-install-nginx-module --auto --prefix /usr/lib/nginx --nginx-source-dir=/tmp/nginx --extra-configure-flags='--with-http_auth_request_module --with-http_realip_module --with-http_upstream_socks_module'
rm -rf /tmp/nginx
#do bundle install
cd /tmp
tar -xzvf tapp.tar.gz
cd tapp
bundle install --without=development test

# Install RVM, Ruby 2.1
install_rvm

cd /tmp
rm -rf /tmp/tapp*
#remove chef client.pem
rm -rf /etc/chef/client.pem
#remove all crontabs
crontab -r

# cleanup
rm -rf /var/lib/cloud/data/scripts/*  # remove part files
rm -rf /usr/lib/qubole/shared
ls -d /usr/lib/qubole/packages/* | grep -v hustler | xargs rm -rf # remove old packages, but not hustler (it is needed by oozie deployment)
rm -rf /usr/lib/nginx.pre-rate-limit  # remove old nginx package

#Setup test_env to run deploy tests
sudo easy_install virtualenv
virtualenv /usr/lib/qubole/test_venv
source /usr/lib/qubole/test_venv/bin/activate
easy_install unittest2
easy_install nose
easy_install pyyaml
