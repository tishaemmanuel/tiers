#!/bin/bash
# chkconfig: 345 99 10
# description: auto start apex listener
#
case "$1" in
 'start')
   su - ec2-user -c "cd /home/ec2-user/; source pypi/bin/activate; uwsgi --daemonize --ini-paste-logged pypi.ini";
   su -c "nginx";;
 'stop')
   echo "put something to shutdown or kill the process here";;
esac