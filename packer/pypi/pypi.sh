#!/bin/bash

cd /tmp
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install virtualenv
sudo yum install -y gcc python-devel nginx w3m

cd ~
virtualenv pypi
source pypi/bin/activate
pip install pypicloud waitress argparse uwsgi

mkdir /home/ec2-user/log

sudo cp /tmp/nginx.conf /etc/nginx/nginx.conf

sudo cp /tmp/init.d /etc/init.d/pypi
sudo chmod +x /etc/init.d/pypi

sudo chkconfig --add pypi
