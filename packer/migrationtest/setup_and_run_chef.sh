#!/bin/bash
devices=`wget -q -O - http://169.254.169.254/latest/meta-data/block-device-mapping/`

for d in $devices
do
    echo $d | grep "ephemeral" > /dev/null
    if [ $? == 0 ]; then
        disk=`wget -q -O - http://169.254.169.254/latest/meta-data/block-device-mapping/$d`
        if [ -b /dev/$disk ]; then
            mkdir -p /media/$d
            mount /dev/$disk /media/$d
            chmod 777 /media/$d
            chmod +t /media/$d
        else
            # the image may contain the /media/eph* points - remove them
            # because some code (sedprops) makes the assumption that each
            # of these is a different drive. For micro instances do not remove
            # ephemeral0
            echo $d | grep "ephemeral0$" > /dev/null
            if [ $? != 0 ]; then
                rm -fr /media/$d
            fi
        fi
    fi
done

chmod 777 /tmp
chmod +t /tmp

for i in /media/ephemeral0/logs /media/ephemeral0/pids /media/ephemeral0/logs/hustler /media/ephemeral0/hadoop-root
do
    mkdir -p $i
    chmod 777 $i
done 


for i in /media/ephemeral0/logs/hustler/debug.log
do
    touch $i
    chown root:hadoop $i
    chmod 777 $i
done

if [ `gem list ohai -i` != "true" ]; then
  gem install ohai --no-rdoc --no-ri --verbose
fi

if [ `gem list chef -i` != "true" ]; then
  gem install chef --no-rdoc --no-ri --verbose
fi

rm -f /etc/chef/client.pem
mkdir -p /etc/chef

mv /home/ec2-user/validation.pem /etc/chef/

mv /home/ec2-user/encrypted_data_bag_secret  /etc/chef/

instance_name=`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`

cat >/tmp/client.rb <<EOF
log_level :info
log_location     "/var/log/chef/client.log"
verbose_logging true
chef_server_url  "http://50.16.240.41:4000"
validation_client_name "qbol-validator"
encrypted_data_bag_secret "/etc/chef/encrypted_data_bag_secret"
node_name       "${instance_name}"
EOF

mv /tmp/client.rb /etc/chef/

rm -rf /var/log/chef
mkdir -p /media/ephemeral0/logs/chef
ln -s /media/ephemeral0/logs/chef /var/log/chef

#chef-client -j /etc/chef/first-boot.json -E multicluster 
chef-client -o "qds_ops_common::migration_testing_instance"

