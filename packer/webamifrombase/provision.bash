#!/bin/bash -xe

# Reason 1: install a known compatible and satisfying version, else the >= dependency on it causes rubygems to install a latest, but incompatible version

function prepare_for_installers() {
    curl -o /etc/yum.repos.d/cloudera-cdh3.repo 'http://archive.cloudera.com/redhat/cdh/cloudera-cdh3.repo'   # add repo to yum
    groupadd --system hadoop                                                                                  # create users and groups
    useradd --system -G hadoop -d /usr/lib/hadoop-0.20 mapred                                                 # create users and groups
    useradd --system -G hadoop -d /usr/lib/hadoop-0.20 hdfs                                                   # create users and groups
}

function install_java() {
    local JDK7_RPM="jdk-7u67-linux-x64.rpm"
    local JDK7_VERSION="jdk1.7.0_67"
    local download_url="http://download.oracle.com/otn-pub/java/jdk/7u67-b01/$JDK7_RPM"
    wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" "$download_url" -O "/tmp/$JDK7_RPM" --no-check-certificate
    rpm -ivh "/tmp/$JDK7_RPM"
    ln -s "/usr/java/$JDK7_VERSION" /usr/lib/jvm/java-1.7.0
    ln -s /usr/lib/jvm/java-1.7.0/bin/jps /usr/bin/jps
}

function install_ruby() {
    cd /usr/local/src
    wget ftp://ftp.ruby-lang.org/pub/ruby/2.1/ruby-2.1.6.tar.gz
    tar zxvf ruby-2.1.6.tar.gz
    cd ruby-2.1.6
    ./configure --prefix=/usr --disable-install-doc --disable-install-rdoc --disable-install-capi
    make
    make install

    cd ..
    wget http://production.cf.rubygems.org/rubygems/rubygems-1.8.25.tgz
    tar zxvf rubygems-1.8.25.tgz
    cd rubygems-1.8.25
    /usr/bin/ruby setup.rb
}

function install_gen_packages() {
    yum erase -y ruby20 ruby20-libs                          # remove ruby2.0 prebaked in ami
    yum install -y gcc gcc-c++                               # needed for building native extensions of ruby gems
    yum install -y glibc.i686                                # our compiled autossh binary in hustler needs it
    yum install -y libxml2 libxml2-devel libxslt libxslt-devel  # why do we need this ?
    yum install -y snappy snappy-devel                       # for hadoop2
    yum install -y lzo lzo-devel
    yum install -y bzip2-devel
    yum install -y mysql mysql-devel
    yum install -y sqlite sqlite-devel
    yum update -y openssh
    # set up python2.6
    yum erase -y python27-pip.noarch
    yum install -y python26-pip.noarch
    yum install -y python26-pycurl
    yum install -y MySQL-python26.x86_64
    yum install -y python-devel python-boto
    alternatives --set python /usr/bin/python2.6

    yum install -y monit
    yum install -y git-all
    yum install -y vim 
    yum install -y hadoop-0.20 hadoop-0.20-datanode hadoop-0.20-namenode hadoop-0.20-jobtracker hadoop-0.20-tasktracker hadoop-0.20-native
    chkconfig --del hadoop-0.20-datanode                     # remove it from chkconfig management, so that they don't start on boot
    chkconfig --del hadoop-0.20-namenode                     # remove it from chkconfig management, so that they don't start on boot
    chkconfig --del hadoop-0.20-jobtracker                   # remove it from chkconfig management, so that they don't start on boot
    chkconfig --del hadoop-0.20-tasktracker                  # remove it from chkconfig management, so that they don't start on boot
    install_ruby
    gem install --no-ri --no-rdoc rake -v 10.1.1             # Reason 1 above. >= by passenger. latest not acceptable by tapp
    gem install --no-ri --no-rdoc passenger -v 4.0.53
    gem install --no-ri --no-rdoc httparty
    gem install --no-ri --no-rdoc aws-sdk

    yum install -y libcurl-devel                             # needed to compile nginx
    passenger-install-nginx-module --auto --prefix /usr/lib/nginx --nginx-source-dir=/tmp/nginx --extra-configure-flags='--with-http_auth_request_module --with-http_realip_module --with-http_upstream_socks_module' # compile nginx
    mv /tmp/etc-init.d-nginx /etc/init.d/nginx
    chown root:root /etc/init.d/nginx                        # /etc/init.d/nginx is used by tapp to start/stop nginx
    chmod 755 /etc/init.d/nginx                              # /etc/init.d/nginx is used by tapp to start/stop nginx

    yum install -y collectd
    yum install -y memcached
    gem install signalfx
    gem install chef-metrics
    easy_install 'pssh'

    #Install logstash
    pushd /tmp
    wget http://assets.push.qubole.com.s3-website-us-east-1.amazonaws.com/logstash-1.4.0.tar.gz
    tar -x -f /tmp/logstash-1.4.0.tar.gz -C /usr/lib
    ln -s /usr/lib/logstash-1.4.0 /usr/lib/logstash
    rm -f /tmp/logstash-1.4.0.tar.gz 
    popd
}

function install_chef_client() {
    gem install --no-ri --no-rdoc chef -v 12.5.1
    gem update --no-ri --no-rdoc mail # update mail to latest version (2.6.3)
    mkdir /etc/chef
    mv /tmp/validation.pem /etc/chef/
    mv /tmp/encrypted_data_bag_secret /etc/chef/
}

function install_app_specific_stuff() {
    # install gems for tapp
    gem install --no-ri --no-rdoc passenger -v 3.0.11
    gem install --no-ri --no-rdoc bundler -v 1.1.1

   # run bundle install
    pushd /tmp
    tar xvf tapp.tar.gz
    pushd tapp
    bundle install --without=development test
    popd
    rm -rf tapp
    rm -rf tapp.tar.gz
    popd
    
    # install packages needed by hive_scripts
    pip install MySQL-python
    pip install s3cmd==1.5.2
    ln -s /usr/local/bin/s3cmd /usr/bin/s3cmd
    pip install spotman_client
    ln -s /usr/local/bin/spotman-client /usr/bin/spotman-client
    pip install PyYAML
    pip uninstall -y pycrypto
    pip install pycrypto==2.5

    # install packages needed by hustler
    easy_install decorator
    easy_install jinja2
    easy_install pyasn1
    easy_install workerpool
    yum install -y libffi-devel                              # required to install gem ffi
    pip install cryptography==1.2.1

    # setup for test_env to run deploy tests
    easy_install virtualenv
    /usr/local/bin/virtualenv /usr/lib/qubole/test_venv
    source /usr/lib/qubole/test_venv/bin/activate
    easy_install unittest2
    easy_install nose
    easy_install pyyaml

    # temporary set up that will finally move into chef
    mkdir -p /usr/lib/nginx/conf
    mkdir -p /opt/logstash/conf
    mkdir -p /opt/logstash/bin
    mkdir -p /etc/monit.d
    echo "export PATH=\"\$PATH\":/usr/lib/hustler/bin" > /etc/profile.d/qubole.sh
}

function install_sec_updates() {
    yum -y --security update
}

function config_ssh() {
    # prevent cloud-init from blocking ssh into the node as root
    cat /etc/ssh/sshd_config | sed s/"^PermitRootLogin forced-commands-only$"/"PermitRootLogin without-password"/ > /tmp/sshd_config
    mv /tmp/sshd_config /etc/ssh/sshd_config
    echo "disable_root: false" > /etc/cloud/cloud.cfg.d/enable_ssh_as_root.cfg

    # configure global ssh client config
    mv /tmp/ssh_config /etc/ssh/ssh_config
    chown root:root /etc/ssh/ssh_config
    chmod 644 /etc/ssh/ssh_config  
}

# the sequence of these is important
prepare_for_installers
install_sec_updates
install_java
install_gen_packages
install_app_specific_stuff
install_chef_client
config_ssh

