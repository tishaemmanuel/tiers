#!/bin/bash
set -ex

JDK8_60_RPM="jdk-8u60-linux-x64.rpm"
JDK8_60_VERSION="jdk1.8.0_60"

if [[ -d /usr/java/$JDK8_60_VERSION ]]; then
  # do not reinstall if already installed
  exit 0
fi

wget --no-check-certificate --no-cookies \
    --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm" \
    -O /tmp/$JDK8_60_RPM --no-check-certificate

rpm -Uvh /tmp/$JDK8_60_RPM

rm -f /usr/lib/jvm/java-1.8.0
mkdir -p /usr/lib/jvm
ln -s /usr/java/$JDK8_60_VERSION /usr/lib/jvm/java-1.8.0

