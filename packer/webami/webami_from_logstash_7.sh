#!/bin/bash

set -ex

function patch_vulnerabilities() {
    yum update -y glibc  # upgrade glibc for CVE-2015-0235
    yum update -y bash
    yum update -y openssl
}
patch_vulnerabilities

# Snappy libs required for hadoop2
yum install -y snappy snappy-devel

#SEC-192 Security fix 
yum update -y openssh-6.6.1p1-12.57.amzn1

#install new nginx
gem install passenger -v 4.0.53
passenger-install-nginx-module --auto --prefix /usr/lib/nginx --nginx-source-dir=/tmp/nginx --extra-configure-flags='--with-http_auth_request_module --with-http_realip_module --with-http_upstream_socks_module'
rm -rf /tmp/nginx
#do bundle install
cd /tmp
tar -xzvf tapp.tar.gz
cd tapp
bundle install --without=development test
cd /tmp
rm -rf /tmp/tapp*
#remove chef client.pem
rm -rf /etc/chef/client.pem

# cleanup 
rm -rf /var/lib/cloud/data/scripts/*  # remove part files
rm -rf /usr/lib/qubole/shared
ls -d /usr/lib/qubole/packages/* | grep -v hustler | xargs rm -rf # remove old packages, but not hustler (it is needed by oozie deployment) 
rm -rf /usr/lib/nginx.pre-rate-limit  # remove old nginx package

easy_install pip
pip install s3cmd==1.5.2
pip install spotman_client --upgrade

# For hustler
yum -y install libffi-devel
pip install cryptography==1.0.2

#Setup test_env to run deploy tests
sudo easy_install virtualenv
/usr/local/bin/virtualenv /usr/lib/qubole/test_venv
source /usr/lib/qubole/test_venv/bin/activate
easy_install unittest2
easy_install nose
easy_install pyyaml


s3cmd get s3://qubole-chef-12/chefdk-0.6.2-1.el6.x86_64.rpm chefdk-0.6.2-1.el6.x86_64.rpm
sudo rpm -Uvh chefdk-0.6.2-1.el6.x86_64.rpm
chef verify
